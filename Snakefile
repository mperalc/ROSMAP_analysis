#!/usr/bin/env python3
# Snakefile for processing ROSMAP Olah single cell data
# snakemake is in /software/teamtrynka/conda/trynka-base/bin/snakemake
import os

SAMPLE = ["Microglia_MO_AD1", "Microglia_MO_AD2", "Microglia_MO_AD3", "Microglia_MO_AD4", "Microglia_MO_AD51",
"Microglia_MO_AD6","Microglia_MO_AD71","Microglia_MO_AD8","Microglia_MO_AD9",
"Microglia_MO_MCI1","Microglia_MO_MCI2","Microglia_MO_MCI3","Microglia_MO_MCI4"]

rule all:
	input:
		matrix=expand("../../data/{sample}/outs/filtered_feature_bc_matrix/matrix.mtx.gz", sample=SAMPLE)

rule cellranger:
	output:
		matrix="../../data/{sample}/outs/filtered_feature_bc_matrix/matrix.mtx.gz"
	message: "Cellranger to process 10X data (no gRNAs). Place all fastqs together in fastqs folder first. Run with snakemake --jobs 5 --cluster [comma] bsub {params.group} {params.queue} {params.threads} {params.memory} {params.jobname} {params.error} [comma]"
	params:
		group= "-G teamtrynka",
		queue="-q normal",
		threads="-n 32",
		memory="-M300000 -R'span[hosts=1] select[mem>300000] rusage[mem=300000]'",
		jobname= "-o ../../logs/log_{sample}_cellranger.%J.%I",
		error="-e ../../errors/error_{sample}_cellranger.%J.%I",
		transcriptome="/software/teamtrynka/cellranger/refdata-gex-GRCh38-2020-A"
	shell:
		"""
		# Need to run in directory that contains the fastq folder to avoid errors
		cd ../../data/
        samples={wildcards.sample}
        echo "processing ${{samples}}"
        # to avoid pipestance directory error:
        rm -r {wildcards.sample}

		# Run cellranger
		/software/teamtrynka/cellranger/cellranger-6.0.1/cellranger count \
        --id={wildcards.sample} \
        --sample=${{samples}} \
        --fastqs=./fastq \
        --transcriptome={params.transcriptome} \
		--localmem=290
		"""
